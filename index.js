function familyFeud(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;


	this.tackle = function(target){
		console.log(this.name + ' '+ 'tackled ' + target.name)
	};

	this.faint = function(){
		console.log(this.name + ' ' + 'fainted')
	};
	this.health = function() {
		console.log(this.name + " " + 'health is reduced to' + " " + 20);
	};
}

// creating instance
let cousin1 = new familyFeud('cousin1', 3, 30);
let cousin2 = new familyFeud('cousin2', 2, 20); 

cousin1.tackle(cousin2);
cousin2.health();
cousin2.tackle(cousin1);
cousin1.faint();
